package com.hcl.dao;

import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.Fruits;

public interface AdminDao {
  public int adminAuthentication(Admin admin);

  public List<Fruits> viewAllFruits();

  public int addFruits(Fruits info);

  public int editInfo(Fruits edit);

  public int removeFruit(Fruits info);

}

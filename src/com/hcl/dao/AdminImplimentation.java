package com.hcl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.Fruits;
import com.hcl.util.DB;
import com.hcl.util.Query;

public class AdminImplimentation implements AdminDao {
  PreparedStatement pst = null;
  ResultSet rs;
  int result;

  @Override
  public int adminAuthentication(Admin admin) {
    result = 0;
    try {
      pst = DB.getConnection().prepareStatement(Query.adminAuth);
      pst.setString(1, admin.getAid());
      pst.setString(2, admin.getPassWord());
      rs = pst.executeQuery();
      while (rs.next()) {
        result++;
      }

    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception oocurs in Admin Authentication");
    } finally {
      try {
        pst.close();
        rs.close();
        DB.getConnection().close();
      } catch (ClassNotFoundException | SQLException e) {
      }

    }
    return result;

  }

  @Override
  public List<Fruits> viewAllFruits() {
    List<Fruits> list = new ArrayList<Fruits>();
    try {
      pst = DB.getConnection().prepareStatement(Query.viewALL);
      rs = pst.executeQuery();
      while (rs.next()) {
        Fruits info = new Fruits(rs.getInt(1), rs.getString(2), rs.getString(3),
            rs.getDouble(4), rs.getString(5), rs.getString(6));
        list.add(info);
      }
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception occurs in view All Fruits");
    } finally {
      try {
        DB.getConnection().close();
        pst.close();
        rs.close();
      } catch (ClassNotFoundException | SQLException e) {

      }
    }
    return list;
  }

  @Override
  public int addFruits(Fruits info) {
    int result = 0;
    try {
      pst = DB.getConnection().prepareStatement(Query.addFruits);
      pst.setInt(1, info.getFid());
      pst.setString(2, info.getFname());
      pst.setString(3, info.getColor());
      pst.setDouble(4, info.getFprice());
      pst.setString(5, info.getFplace());
      pst.setString(6, info.getFquantity());
      result = pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception occurs in Add new Fruits");
    } finally {
      try {
        DB.getConnection().close();
        pst.close();
      } catch (ClassNotFoundException | SQLException e) {

      }

    }

    return result;
  }

  @Override
  public int editInfo(Fruits edit) {
    int result = 0;
    try {
      pst = DB.getConnection().prepareStatement(Query.editInfo);
      pst.setString(1, edit.getFplace());
      pst.setDouble(2, edit.getFprice());
      pst.setString(3, edit.getFquantity());
      pst.setInt(4, edit.getFid());
      result = pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception occurs in edit fruits");
    } finally {
      try {
        DB.getConnection().close();
        pst.close();
      } catch (ClassNotFoundException | SQLException e) {

      }

    }

    return result;
  }

  @Override
  public int removeFruit(Fruits info) {
    try {
      pst = DB.getConnection().prepareStatement(Query.removeFruits);
      pst.setInt(1, info.getFid());
      result = pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception Occurs in Remove fruits");
    }finally {
      try {
        DB.getConnection().close();
        pst.close();
      } catch (ClassNotFoundException | SQLException e) {
        
      }
    }
    return 0;
  }

}

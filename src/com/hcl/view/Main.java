package com.hcl.view;

import java.util.List;
import java.util.Scanner;

import com.hcl.controller.AdminController;
import com.hcl.model.Fruits;

public class Main {

  public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    System.out.println("Enter user aid");
    String aid = s.nextLine();
    System.out.println("Enter user password");
    String password = s.nextLine();
    AdminController controller = new AdminController();
    int result = 0;
    result = controller.adminAuthentication(aid, password);
    if (result > 0) {
      System.out.println("Hi " + aid + ", Welcome to Admin Page");
      int con = 0;
      do {
        System.out.println(
            "1) Add new fruits  2) Remove Fruits 3) Edit Fruits  4) View All Fruits");
        int option = s.nextInt();
        int id = 0;
        String name = "";
        String color = "";
        Double price;
        String place = "";
        String q = "";
        switch (option) {
        case 1:
          System.out.println("Enter fruit id");
          id = s.nextInt();
          s.nextLine();
          System.out.println("Enter fruit name");
          name = s.nextLine();
          System.out.println("Enter fruit color");
          color = s.nextLine();
          System.out.println("Enter fruit price");
          price = s.nextDouble();
          s.nextLine();
          System.out.println("Enter fruit place");
          place = s.nextLine();
          System.out.println("Enter fruit quantity");
          q = s.nextLine();
          result = controller.addFruits(id, name, color, price, place, q);
          if (result > 0) {
            System.out.println(id + " Successfully inserted");
          }
          break;
        case 2:
          System.out.println("Enter Fruit id remove from database");
          id = s.nextInt();
          result = controller.removeFruit(id);

          System.out.println((result > 0) ? id + " remove was not found "
              : id + " Remove Successfully ");

          break;
        case 3:
          System.out.println("Enter fruits id");
          id = s.nextInt();
          System.out.println("Enter fruits price");
          price = s.nextDouble();
          s.nextLine();
          System.out.println("Enter fruits place");
          place = s.nextLine();
          System.out.println("Enter fruits quantity");
          q = s.nextLine();
          result = controller.editInfo(id, price, place, q);
          if (result > 0) {
            System.out.println(id + "  Successfully Updated");
          }
          break;
        case 4:
          List<Fruits> list = controller.viewAllFruits();
          if (list.size() > 0) {
            System.out.println("fid,fname,color,fprice,fplace,fquantity");
            for (Fruits fruits : list) {
              System.out.println(fruits.getFid() + "," + fruits.getFname() + ","
                  + fruits.getColor() + "," + fruits.getFprice() + ","
                  + fruits.getFplace() + "," + fruits.getFquantity());

            }

          } else {
            System.out.println("No records found");
          }
          break;
        default:
          System.out.println("invalid option");
        }
        System.out.println("Do you want to continue press 1");
        con = s.nextInt();
      } while (con == 1);
    } else {
      System.out.println("Your user id or password invalid");
    }
    s.close();

  }

}

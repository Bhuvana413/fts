package com.hcl.controller;

import java.util.List;

import com.hcl.dao.AdminDao;
import com.hcl.dao.AdminImplimentation;
import com.hcl.model.Admin;
import com.hcl.model.Fruits;

public class AdminController {
  int result;
  AdminDao dao = new AdminImplimentation();

  public int adminAuthentication(String aid, String password) {
    result = 0;
    Admin admin = new Admin(aid, password);
    return dao.adminAuthentication(admin);

  }

  public List<Fruits> viewAllFruits() {
    return dao.viewAllFruits();

  }

  public int addFruits(int fid, String fname, String color, double fprice,
      String fplace, String fquantity) {
    Fruits info = new Fruits(fid, fname, color, fprice, fplace, fquantity);
    return dao.addFruits(info);
  }
  public int editInfo(int fid,Double fprice,String fplace,String fquantity) {
    Fruits edit =new Fruits();
    edit.setFid(fid);
    edit.setFplace(fplace);
    edit.setFprice(fprice);
    edit.setFquantity(fquantity);
    return dao.editInfo(edit);
    
  }
  public int removeFruit(int fid) {
    Fruits info = new Fruits();
    info.setFid(fid);
    return dao.removeFruit(info);
    
  }

}

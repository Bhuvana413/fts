package com.hcl.model;

public class Admin {
  private String aid;
  private String passWord;
  public Admin() {
    // TODO Auto-generated constructor stub
  }
  public Admin(String aid, String passWord) {
    super();
    this.aid = aid;
    this.passWord = passWord;
  }
  public String getAid() {
    return aid;
  }
  public void setAid(String aid) {
    this.aid = aid;
  }
  public String getPassWord() {
    return passWord;
  }
  public void setPassWord(String passWord) {
    this.passWord = passWord;
  }
  
  

}
